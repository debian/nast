fixes escaping of minus signs in man pages to fix copying and pasting
in UTF-8. See
http://lintian.debian.org/tags/hyphen-used-as-minus-sign.html

--- a/nast.8
+++ b/nast.8
@@ -20,7 +20,7 @@
 nast \- Network Analyzer Sniffer Tool
 
 .SH SYNOPSIS
-nast [-G] [-i interface] [-l filename] [-f filter] [--ld filename] [-pdxPmsgrSMLbcCBVh]
+nast [\-G] [\-i interface] [\-l filename] [\-f filter] [\-\-ld filename] [\-pdxPmsgrSMLbcCBVh]
 
 .SH DESCRIPTION
 Nast is a packet sniffer and a LAN analyzer based on Libnet and Libpcap.
@@ -60,41 +60,41 @@
 .PP
 .SH CMDLINE SNIFFER OPTIONS
 .TP
-\fB-i, --interface\fR
+\fB\-i, \-\-interface\fR
 Select the Interface, if not specified will be auto-detected.
 .br
 .TP
-\fB-p, --promisc\fR
+\fB\-p, \-\-promisc\fR
 Disable promiscuous mode on NIC.
 .br
 .TP
-\fB-d, --ascii-data\fR
+\fB\-d, \-\-ascii-data\fR
 Print data in ascii format.
 .br
 .TP
-\fB-x, --ascii-hex-data\fR
+\fB\-x, \-\-ascii-hex-data\fR
 Print data in ascii-hex format.
 .br
 .TP
-\fB-f, --filter <"filter">\fR
+\fB\-f, \-\-filter <"filter">\fR
 Apply <"filter"> to sniffer (see "FILTER SYNTAX" section below for syntax)
 .br
 .TP
-\fB    --ld <filename>\fR
-Log captured data to <filename> (only payload). Use -l to log all packet instead, useful with -B
+\fB    \-\-ld <filename>\fR
+Log captured data to <filename> (only payload). Use \-l to log all packet instead, useful with \-B
 .br
 .TP
-\fB-T, --tcpdump-log <filename>\fR
+\fB\-T, \-\-tcpdump-log <filename>\fR
 Log all packets in tcpdump format to <filename>
 .br
 .TP
-\fB-R, --tcpdump-log-read <filename>\fR
+\fB\-R, \-\-tcpdump-log-read <filename>\fR
 Read all packets saved in tcpdump format from <filename>
 .br
 .PP
 .SH ANALYZER FEATURES
 .TP
-\fB-P, --check-promisc <ip>\fR
+\fB\-P, \-\-check-promisc <ip>\fR
 Check other NIC on the LAN with the promiscuous flag set.
 .br
 By performing a fake ARP broadcast, we can determine if a NIC is in promiscuous mode or not. 
@@ -104,7 +104,7 @@
 .br
 Use \fB-P all\fR to query all network NIC
 
-eg: root@localhost:~/$ nast -P 192.168.1.2
+eg: root@localhost:~/$ nast \-P 192.168.1.2
 
 NAST "NETWORK ANALYZER SNIFFER TOOL"
 
@@ -112,14 +112,14 @@
 
 We can check all nodes by using:
 .br
-root@localhost:~/$ nast -P all
+root@localhost:~/$ nast \-P all
 
 .TP
-\fB-m, --host-list\fR
+\fB\-m, \-\-host-list\fR
 Map the LAN by performing a series of ARP request to sequential subnet IP
 addresses.
 
-eg: root@localhost:~/$ nast -m
+eg: root@localhost:~/$ nast \-m
 
 NAST "NETWORK ANALYZER SNIFFER TOOL"
 
@@ -137,12 +137,12 @@
 
 .br
 .TP
-\fB-s, --tcp-stream\fR
+\fB\-s, \-\-tcp-stream\fR
 Follow a TCP/IP connection printing all data in payload. You must specify the IP addresses of the ends.
 
 eg of a ftp connection:
 .br
-root@localhost:~/$ nast -s
+root@localhost:~/$ nast \-s
 
 NAST "NETWORK ANALYZER SNIFFER TOOL"
 
@@ -178,18 +178,18 @@
 .br
 .br
 .TP
-\fB-g, --find-gateway\fR
+\fB\-g, \-\-find-gateway\fR
 Try to find possible Internet-gateways.
 .br
 We send a SYN packet to a public host on port 80 through sequential host-lan and if a SYN-ACK
 return we have find the gateway.
 .br
 .TP
-\fB-r, --reset-connection\fR
+\fB\-r, \-\-reset-connection\fR
 Destroy an established connection. You must specify the IP addresses of the ends and at least one port . 
 Please, pay attention when use this function.
 
-eg: root@localhost:~/$ nast -r
+eg: root@localhost:~/$ nast \-r
 
 NAST "NETWORK ANALYZER SNIFFER TOOL"
 
@@ -218,7 +218,7 @@
 mechanism works with them.
 .br
 .TP
-\fB-S, --port-scanner\fR
+\fB\-S, \-\-port-scanner\fR
 Performs a half-open port scanning on the selected host. It tries also to determine some firewall (just iptables) rules.
 .br
 About this technique NMAP says:
@@ -231,7 +231,7 @@
 log it.  Unfortunately you need root privileges to build these custom SYN packets.
 .br
 
-eg: root@localhost:~/$ nast -S
+eg: root@localhost:~/$ nast \-S
 .br
 .br
 NAST "NETWORK ANALYZER SNIFFER TOOL"
@@ -264,11 +264,11 @@
 .br
 
 .TP
-\fB-M, --multi-port-scanner\fR
+\fB\-M, \-\-multi-port-scanner\fR
 Same as above but done on all hosts of the lan.
 .br
 .TP
-\fB-L, --find-link\fR
+\fB\-L, \-\-find-link\fR
 Tries to determine what type of link is used in the LAN (Hub or switch).
 .br
 In the LAN segment is there a HUB or a SWITCH? We can find it by sending a
@@ -276,13 +276,13 @@
 at least one of them must reply with a ICMP echo-replay)
 .br    
 .TP
-\fB-b, --daemon-banner\fR
+\fB\-b, \-\-daemon-banner\fR
 Checks the most famous daemon banner on the LAN's hosts.
 .br
 You can customize ports database adding them to ports[] variable in main.c
 .br
 .TP
-\fB-c, --check-arp-poisoning\fR
+\fB\-c, \-\-check-arp-poisoning\fR
 Control ARP answers to discover possible ARP spoofing attacks like
 man-in-the-middle
 .br
@@ -292,12 +292,12 @@
 nobody is making ARP-poisoning, than have fun and relax and check program output:).    
 .br
 .TP
-\fB-C, --byte-counting <"filter">\fR
+\fB\-C, \-\-byte-counting <"filter">\fR
 Apply traffic counting to <"filter"> (see FILTER SYNTAX section below for syntax)
 .br
 Use \fB-C any\fR if you don't want to use a filter.
 
-eg: root@localhost:~/$ nast -C any
+eg: root@localhost:~/$ nast \-C any
 
 NAST "NETWORK ANALYZER SNIFFER TOOL"
 
@@ -313,19 +313,19 @@
 .PP
 .SH GENERAL OPTIONS
 .TP
-\fB-G, --ncurses\fR
+\fB\-G, \-\-ncurses\fR
 Run Nast with the ncurses interfaces (only if compiled with ncurses support)
 .br
 .TP
-\fB-l, --log-file <filename>\fR
+\fB\-l, \-\-log-file <filename>\fR
 Log reports to <filename>. Work with many features.
 .br
 .TP
-\fB-B, --daemon\fR
+\fB\-B, \-\-daemon\fR
 Run in background like daemon and turn off stdout (very useful for sniffer/stream/ARP control logging)
 .br
 .TP
-\fB-V, --version\fR
+\fB\-V, \-\-version\fR
 Show version information
 .PP
 .SH NCURSES INTERFACE NOTE
@@ -707,23 +707,23 @@
 Here are some examples of the use of NAST:
 .br
 .SH
-   nast -f "src 192.168.1.2"
+   nast \-f "src 192.168.1.2"
 .br
 In this example with the help of the filter we choose to see only the
 traffic from 192.168.1.2
 .br
 .SH
-   nast -p -B --ld logfile.txt
+   nast \-p \-B \-\-ld logfile.txt
 .br
 Here we run nast in background mode and log all data that pass through our NIC.
 .br
 .SH
-   nast -S -l logfile.txt
+   nast \-S \-l logfile.txt
 .br
 In this other case we log the results of the port scanner in the file "logfile.txt"
 .br
 .SH
-   nast -c -B
+   nast \-c \-B
 .br
 This is a very useful options. We run in background mode nast that checks if someone
 is arp-poisoning.
